const { calculateSum, randomInt } = require('../src/calc-lib.js');

test('addition', () => {
  expect(calculateSum(2,2)).toBe(4);
});

test('random numbers', () => {
  expect(randomInt(100)).toBeGreaterThanOrEqual(0);
  expect(randomInt(100)).toBeLessThanOrEqual(100);
});
